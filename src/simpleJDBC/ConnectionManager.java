

import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionManager {

    private static final String serverTimeZone = "UTC";
    private static final String serverName = "localhost";
    private static final String databaseName="scheme_name";
    private static final int portNumber = 3306;  //3306 by default in MySQL workbench settings
    private static final String user = "root";
    private static final String password = "admin";

    public static Connection getConnection() throws SQLException {

        //

        MysqlDataSource dataSource = new MysqlDataSource();

        dataSource.setUseSSL(false);
        dataSource.setServerTimezone(serverTimeZone);
        dataSource.setServerName(serverName);
        dataSource.setDatabaseName(databaseName);
        dataSource.setPortNumber(portNumber);
        dataSource.setUser(user);
        dataSource.setPassword(password);

        return dataSource.getConnection();
    }

}
