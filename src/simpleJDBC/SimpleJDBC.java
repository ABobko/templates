package simpleJDBC;

import java.sql.*;

public class SimpleJDBC {


    public static void main(String[] args) {
        /*
         1. Add MySQL data driver to class path
            file->project structure->libraries->"+"->mysql-connector-java-8.0.11.jar
            or add DEPENDENCY to MAVEN
        */

        try {
            //2. get connection
            /*
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3304/test_scheme",
                    "root", "admin");
            */
            Connection conn = ConnectionManager.getConnection();
            //3. create a statement
            Statement stmt = conn.createStatement();

            //4. execute SQL querry
            ResultSet result = stmt.executeQuery("SELECT * FROM table_name");

            //executeUpate doesn't return resultSet
            //stmt.executeUpdate("INSERT INTO scheme_name.db_name VALUES (844, \'string data\', \'2010-11-01\')");

            //make use of ResultSet
            while (result.next()) {
                System.out.println(result.getString("acname") + " until: "
                        + result.getString("expdate")); //change System.out.println to other logic
            }

            //ALWAYS close the connection!
            conn.close();   //can use try-with-resources instead
        } catch (Exception e) {
            e.printStackTrace(); //handle the exception
        }
    }
}
