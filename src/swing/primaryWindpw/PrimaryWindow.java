package swing.primaryWindpw;

import javax.swing.*;

public class PrimaryWindow extends JFrame {
    public PrimaryWindow(int width, int height){
        super();
        setTitle("enter title");
        MainDisplay screen = new MainDisplay(width, height);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(screen);
        setVisible(true);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
    }
}
