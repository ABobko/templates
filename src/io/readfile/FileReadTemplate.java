package io.readfile;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileReadTemplate {

    public static void main(String[] args) throws URISyntaxException {
        URI templateFileUri = FileReadTemplate.class
                .getResource("yourFile.txt").toURI();
        Path templateFile = Paths.get(templateFileUri);

        Path homePath = Paths.get("C:", "Users",
                "youruser", "dirName",
                "subDirName",
                "anotherSubDir","etc");     //c:\Users\dirName\subDirName\anotherSubDir\etc

        Path filePath = homePath.resolve("yourFile.txt");

    }


    private static void readTextFileJava8(Path filePath) {
        try {
            List<String> lines = Files.readAllLines(filePath);

             lines.stream().forEach(line -> System.out.println(line));  //replace System.out.println with logic
             lines.forEach(System.out::println);                        //internal iteration on each element,

            //second way
            Files.readAllLines(filePath, Charset.forName("ISO-8859-5"))
                    .forEach(System.out::println);                      //replace System.out with logic
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //most effective way to read HUGE files (1GB if lack of memory,2GB 10GB... etc)
    private static void readTextFileJava7(Path filePath) {
        try (
                Reader reader = new FileReader(filePath.toFile());
                BufferedReader bufferedReader = new BufferedReader(reader)
        ) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);       //replace with logic
            }

        } catch (IOException e) {
            //handle IO exception
        }
    }

    //almost as old as middle ages, avoid due to complexity
    private static void readTextFileJava6(Path filePath) {
        Reader reader = null;
        BufferedReader bufferedReader = null;

        // File file = new File(filePath.toString());
        File file = filePath.toFile();
        try {
            reader = new FileReader(file);
            bufferedReader = new BufferedReader(reader);

            String line = bufferedReader.readLine();
            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("file not found exception");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO exception");
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
